'use strict';
 
var express = require('express'); //install express
var path = require('path');
var bodyParser = require('body-parser'); //when we use use frontend and request
                                        //for post mean send data from frontend this work as a middleware
var cors = require('cors');

var app = express();  //express app intiate to app
app.use(cors());
app.use(bodyParser.json()); //this app use bodyparser 
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static(path.join(__dirname, '../client'))); // In client folder we have frontend code

require('./db');
require('./routes')(app);

app.set('port', process.env.PORT || 3000); //set port to default port 3000
 
var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});

exports = module.exports = app;